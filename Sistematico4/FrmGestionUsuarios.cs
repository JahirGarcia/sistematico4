﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistematico4
{
    public partial class FrmGestionUsuarios : Form
    {
        private DataSet dsUsuarios;
        private BindingSource bsUsuarios;

        public FrmGestionUsuarios()
        {
            InitializeComponent();
            bsUsuarios = new BindingSource();
        }

        public DataSet DsUsuarios
        {
            set
            {
                dsUsuarios = value;
            }
        }

        private void FrmGestionUsuarios_Load(object sender, EventArgs e)
        {
            bsUsuarios.DataSource = dsUsuarios;
            bsUsuarios.DataMember = dsUsuarios.Tables["Usuario"].TableName;

            dgvUsuarios.DataSource = bsUsuarios;
            dgvUsuarios.AutoGenerateColumns = true;
        }

        private void txtFilter_TextChanged(object sender, EventArgs e)
        {
            bsUsuarios.Filter = string.Format("Nombre like '%{0}%' or Correo like '%{0}%' or Telefono like '%{0}%'", txtFilter.Text);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            FrmUsuario fu = new FrmUsuario();
            fu.DtUsuarios = dsUsuarios.Tables["Usuario"];
            fu.ShowDialog();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DataRow row = getSelectedRow();

            if (row != null)
            {
                FrmUsuario fe = new FrmUsuario();
                fe.DrUsuario = row;
                fe.ShowDialog();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DataRow row = getSelectedRow();

            if (row != null)
            {
                DialogResult result = MessageBox.Show("Seguro que deseas eliminar este usuario?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                if (result == DialogResult.Yes)
                {
                    dsUsuarios.Tables["Usuario"].Rows.Remove(row);
                    MessageBox.Show("El Usuario ha sido eliminado satisfactoreamente!", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private DataRow getSelectedRow()
        {
            DataGridViewSelectedRowCollection rowCollecction = dgvUsuarios.SelectedRows;

            if (rowCollecction.Count == 0)
            {
                MessageBox.Show("Selecciona primero una fila de la tabla", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }

            DataGridViewRow gridRow = rowCollecction[0];
            DataRowView rowView = (DataRowView)gridRow.DataBoundItem;

            if (rowView != null)
            {
                DataRow row = rowView.Row;
                return row;
            }
            else
            {
                return null;
            }

        }
    }
}
