﻿using Sistematico4.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistematico4
{
    public partial class FrmUsuario : Form
    {

        private DataTable dtUsuarios;
        private DataRow drUsuario;

        public DataTable DtUsuarios
        {
            set
            {
                dtUsuarios = value;
            }
        }

        public DataRow DrUsuario
        {
            set
            {
                drUsuario = value;
            }
        }

        public FrmUsuario()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string nombre, contrasena, confContrasena, correo, confCorreo, telefono, confTelefono;
            int accesosFallidos;

            nombre = txtName.Text;
            contrasena = txtPassword.Text;
            confContrasena = txtConfirmPass.Text;
            correo = mskEmail.Text;
            confCorreo = mskConfirmEmail.Text;
            telefono = mskPhone.Text;
            confTelefono = mskCinfirmPhone.Text;
            accesosFallidos = (int)nudAccess.Value;

            if(contrasena.Equals(confContrasena) && correo.Equals(confCorreo) && telefono.Equals(confTelefono))
            {
                if(drUsuario != null)
                {
                    drUsuario["Nombre"] = nombre;
                    drUsuario["Correo"] = correo;
                    drUsuario["Contrasena"] = contrasena;
                    drUsuario["Telefono"] = telefono;
                    MessageBox.Show("El Usuario ha sido actualizado correctamente!", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                } else
                {
                    int id = dtUsuarios.Rows.Count + 1;
                    dtUsuarios.Rows.Add(id, nombre, correo, telefono, contrasena);
                    MessageBox.Show("El usuario se ha agregado correctamente!", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                Dispose();
            } else
            {
                MessageBox.Show("Los campos no coinciden, verifiquelos correctamente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                nudAccess.Value++;
            }
        }

        private void FrmUsuario_Load(object sender, EventArgs e)
        {
            setupFileds();
        }

        private void setupFileds()
        {
            if (drUsuario != null)
            {
                txtName.Text = drUsuario["Nombre"].ToString();
                txtPassword.Text = drUsuario["Contrasena"].ToString();
                mskEmail.Text = drUsuario["Correo"].ToString();
                mskPhone.Text = drUsuario["Telefono"].ToString();
            }
        }
    }
}
