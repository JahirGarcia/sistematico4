﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistematico4.Entities
{
    class Usuario
    {
        private int id;
        private string nombreUsuario;
        private string contrasena;
        private string confirmarContrasena;
        private string correo;
        private string confirmarCorreo;
        private string telefono;
        private string confirmarTelefono;
        private int intentosFallidos;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string NombreUsuario
        {
            get
            {
                return nombreUsuario;
            }

            set
            {
                nombreUsuario = value;
            }
        }

        public string Contrasena
        {
            get
            {
                return contrasena;
            }

            set
            {
                contrasena = value;
            }
        }

        public string ConfirmarContrasena
        {
            get
            {
                return confirmarContrasena;
            }

            set
            {
                confirmarContrasena = value;
            }
        }

        public string Correo
        {
            get
            {
                return correo;
            }

            set
            {
                correo = value;
            }
        }

        public string ConfirmarCorreo
        {
            get
            {
                return confirmarCorreo;
            }

            set
            {
                confirmarCorreo = value;
            }
        }

        public string Telefono
        {
            get
            {
                return telefono;
            }

            set
            {
                telefono = value;
            }
        }

        public string ConfirmarTelefono
        {
            get
            {
                return confirmarTelefono;
            }

            set
            {
                confirmarTelefono = value;
            }
        }

        public int IntentosFallidos
        {
            get
            {
                return intentosFallidos;
            }

            set
            {
                intentosFallidos = value;
            }
        }

        public Usuario(int id, string nombreUsuario, string contrasena, string confirmarContrasena, string correo, string confirmarCorreo, string telefono, string confirmarTelefono, int intentosFallidos)
        {
            this.Id = id;
            this.NombreUsuario = nombreUsuario;
            this.Contrasena = contrasena;
            this.ConfirmarContrasena = confirmarContrasena;
            this.Correo = correo;
            this.ConfirmarCorreo = confirmarCorreo;
            this.Telefono = telefono;
            this.ConfirmarTelefono = confirmarTelefono;
            this.IntentosFallidos = intentosFallidos;
        }
    }
}
