﻿using Sistematico4.Entities;
using Sistematico4.Seeders;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistematico4
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void usuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionUsuarios fgu = new FrmGestionUsuarios();
            fgu.MdiParent = this;
            fgu.DsUsuarios = dsUsuarios;
            fgu.Show();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            UsuariosSeeder.Seed();
            foreach(Usuario u in UsuariosSeeder.GetUsuarios())
            {
                dsUsuarios.Tables["Usuario"].Rows.Add(u.Id, u.NombreUsuario, u.Correo, u.Telefono, u.Contrasena);
            }
        }
    }
}
