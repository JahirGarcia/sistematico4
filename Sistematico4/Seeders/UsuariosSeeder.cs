﻿using Sistematico4.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistematico4.Seeders
{
    class UsuariosSeeder
    {
        private static List<Usuario> usuarios = new List<Usuario>();

        public static List<Usuario> GetUsuarios()
        {
            return usuarios;
        }

        public static void Seed()
        {
            Usuario[] usr =
            {
                new Usuario(1, "jahirgarcia", "123456", "123456", "jahirgrcia66@gmail.com", "jahirgrcia66@gmail.com", "86172557", "86172557", 0)
            };

            usuarios = usr.ToList();
        }

    }
}
